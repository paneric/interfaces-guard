<?php

declare(strict_types=1);

namespace Paneric\Interfaces\Guard;

interface GuardInterface
{
    public function encrypt(string $data): ?string;

    public function decrypt(string $data): ?string;

    public function hashPassword(string $password): string;

    public function hash(string $chain): string;

    public function verifyHash(string $registeredHash, string $receivedHash): bool;

    public function verifyPassword(string $password, string $hash): bool;

    public function setUniqueId(): string;

    public function generateRandomString(int $randomStringLength): string;

    public function generatePublicPrivateKeysPair(): array;

    public function encryptWithPublicKey(string $publicKey, string $data): string;

    public function decryptWithPrivateKey(string $privateKey, string $data): string;

    public function encodeJwtToken(array $payload): string;

    public function decodeJwtToken(string $jwt): array;
}
